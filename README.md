# statit

```
        ___   ___   _ 
__   __/ _ \ / _ \ / |
\ \ / / | | | | | || |
 \ V /| |_| | |_| || |
  \_/  \___(_)___(_)_|
                      
```

![image](./icon.png)

# Goal

Just a statistic toy based on psql databases, SQL request are stored directly in code as far as there will never another SQL data based linked to this toy.

It display a single menu of N choices of requests & displays only results

# Structure of code

[structure](./autodoc.doc)

# License

[LICENSE](./LICENSE)
